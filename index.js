#!/usr/bin/env node
const program = require('commander');

program
  .option('-c, --cheese <type>', 'add the specified type of cheese', 'blue')
  .option('-t, --thuan', 'Thuan is so handsome');

program.version("2.0.1")

program.parse(process.argv);